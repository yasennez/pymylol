# delaem белый экран и попиксельно сравниваем картинку
import PIL.Image as Image
import PIL.ImageDraw as ImageDraw


file1 = r"E:\ForPP\scr1.bmp"
compareFile = r"E:\ForPP\scr3.bmp"
diffFile = r"E:\ForPP\scr3rere.bmp"

# Image one
img = Image.open(file1)
width = img.size[0]
height = img.size[1]
pix = img.load()
draw = ImageDraw.Draw(img)

# Image two
img2 = Image.open(compareFile)
width2 = img2.size[0]
height2 = img2.size[1]
pix2 = img2.load()

for i in range(width):
    for j in range(height):
        r = pix[i, j][0]
        g = pix[i, j][1]
        b = pix[i, j][2]

        r2 = pix2[i, j][0]
        g2 = pix2[i, j][1]
        b2 = pix2[i, j][2]

        S1 = r + g + b
        S2 = r2 + g2 + b2

        if S1 != S2:
            draw.point((i, j), (r2, g2, b2))
        else:
            draw.point((i, j), (255, 255, 255))

del draw

img.save(diffFile)


'''  
S = a + b + c
      if S > (((255 + 170) / 2) * 3):
          a, b, c = 255, 255, 255
      else:
          a, b, c = 0, 0, 0
      draw.point((i, j), (255, 100, 255))
'''
